FROM ruby:2.3.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /aaaa
WORKDIR /aaaa

ADD Gemfile /aaaa/Gemfile
ADD Gemfile.lock /aaaa/Gemfile.lock

RUN bundle install

ADD . /aaaa
