require 'csv'
class Registre < ApplicationRecord

  belongs_to :user

  def self.to_csv
    attributes = %w{id created_at tipus}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |registre|
        csv << attributes.map do |attr|
          if attr == 'created_at'
            registre.created_at.localtime.to_formatted_s(:short)
          else
            registre.send(attr)
          end
        end
      end
    end
  end

end
