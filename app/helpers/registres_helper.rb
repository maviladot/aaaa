module RegistresHelper

  def entrada_marcada
    current_day = Time.now.strftime("%F")
    !entrada = @current_user.registres.detect { |registre| registre.created_at.strftime("%F") == current_day and registre.tipus == 'entrada' }.nil?
  end

  def sortida_marcada
    current_day = Time.now.strftime("%F")
    sortida = @current_user.registres.detect { |registre| registre.created_at.strftime("%F") == current_day and registre.tipus == 'sortida' }
    !entrada_marcada || !sortida.nil?
  end

end
