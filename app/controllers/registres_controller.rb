class RegistresController < ApplicationController

  before_action :require_login

  def index
    regisres = Registre.all
    render json: regisres
  end

  def create
    registre = Registre.new(registre_params)
    if registre.save
      redirect_to '/'
    else
      render json: {status: 'ERROR', message: registre.errors},status: :unprocessable_entity
    end
  end

  def to_csv
    user = User.find(params[:id])
    registres = user.registres

    respond_to do |format|
      format.html
      format.csv { send_data registres.to_csv, filename: "#{user.name}-#{Date.today}.csv" }
    end

    #redirect_to user_path(params[:id])
  end

  def destroy
  end

  def show
  end

  def update
  end

  private

  def registre_params
    params.permit(:tipus).merge(user_id: @current_user.id)
  end

end
