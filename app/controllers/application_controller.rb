class ApplicationController < ActionController::Base

  include SessionsHelper

  helper_method :authenticate_admin

  def require_login
    if !logged_in?
      redirect_to "/login"
    end
  end

  def authenticate_admin
    unless @current_user.role == 1
      render json: {error: 'Not Authorized'}, status: :unauthorized
    end
  end
end
