class SessionsController < ApplicationController

  def index
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to '/'
    else
      render 'new'
    end
  end

  def new
  end

  def show
  end

  def update
  end

  def logout
  end

  def destroy
    #log_out
    redirect_to "/login"
  end

  private

  def user_params
  end

end
