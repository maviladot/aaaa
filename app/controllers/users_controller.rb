class UsersController < ApplicationController

  #before_action :authenticate_admin, only: [:index, :update, :destroy]
  before_action :require_login, only: [:index, :update, :destroy, :show]


  def index
    @users = User.all
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: {status: 'SUCCESS', message:'User Created'},status: :ok
    else
      render json: {status: 'ERROR', message:@user.errors},status: :unprocessable_entity
    end
  end

  def destroy
  end

  def show
    @user = User.find(params[:id])
  end

  def update

  end


  private

  def user_params
    params.permit(:email, :name, :password, :password_confirmation, :role)
  end

end
