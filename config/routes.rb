Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :registres
  resources :users

  get '', to: 'static_pages#home'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy'


  get 'csv/:id', to: 'registres#to_csv', :as => 'csv', defaults: { format: 'csv'}



  post 'authenticate', to: 'authentication#authenticate'

end
