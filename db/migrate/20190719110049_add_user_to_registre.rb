class AddUserToRegistre < ActiveRecord::Migration[5.0]
  def change
    add_reference :registres, :user, foreign_key: true
  end
end
